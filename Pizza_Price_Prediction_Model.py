#!/usr/bin/env python
# coding: utf-8

# # Pizza Prize Prediction using XGBRegressor
# #By- Aarush Kumar
# #Dated: August 21,2021

# In[1]:


from IPython.display import Image
Image(url='https://images.unsplash.com/photo-1513104890138-7c749659a591?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxleHBsb3JlLWZlZWR8MXx8fGVufDB8fHx8&w=1000&q=80')


# ## Importing Libraries

# In[2]:


import pandas as pd
pd.set_option('display.width', 100)
pd.set_option('display.max_columns', 8)
import numpy as np
import math
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.model_selection import train_test_split
from sklearn import metrics
from xgboost import XGBRegressor
from sklearn.preprocessing import LabelEncoder
from sklearn.pipeline import Pipeline


# ## Import The Dataset

# In[3]:


dataPizza = pd.read_csv('/home/aarush100616/Downloads/Projects/Pizza Price Prediction/pizza_v1.csv')


# In[4]:


dataPizza


# ## Cleaning & Checking The Dataset

# In[5]:


dataPizza.head()


# In[6]:


dataPizza.tail()


# In[7]:


dataPizza.info()


# In[8]:


price = []

for item in dataPizza['price_rupiah']:
    price += [float(item.replace('Rp', '').replace(',', ''))]
dataPizza['price_rupiah'] = price


# In[9]:


# Seeing the categorical data
dataPizza.loc[:, ['company', 'topping', 'variant', 'size', 'extra_sauce', 'extra_cheese']]


# ## Encode

# In[10]:


labelEncoder = LabelEncoder()


# In[11]:


# Accommodate data into dataVariables
data = dataPizza

# Encode the object data to type int
for e in data.columns:
    if data[e].dtype == 'object':
        labelEncoder.fit(list(data[e].values))
        data[e] = labelEncoder.transform(data[e].values)

        # Change the data type to float
        for i in data.columns:
            if data[i].dtype == 'int':
                data[i] = data[i].astype('float64')

                # Accommodate the data that has been changed
                dataPizza = data


# In[12]:


dataPizza.describe()


# ### Coefficient of Variation

# In[13]:


# Coefficient of Variation Company
covCompany = ((dataPizza['company'].std()/dataPizza['company'].mean()) * 100)
print(f'Coefficient Of Variation Company : {covCompany}%')
# Coefficient of Variation Price
covPrice = ((dataPizza['price_rupiah'].std()/dataPizza['price_rupiah'].mean()) * 100)
print(f'Coefficient Of Variation Price : {covPrice}%')
# Coefficient of Variation Diameter
covDiameter = ((dataPizza['diameter'].std()/dataPizza['diameter'].mean()) * 100)
print(f'Coefficient Of Variation Diameter : {covDiameter}%')
# Coefficient of Variation Topping
covTopping = ((dataPizza['topping'].std()/dataPizza['topping'].mean()) * 100)
print(f'Coefficient Of Variation Topping : {covTopping}%')
# Coefficient of Variation Size
covSize = ((dataPizza['size'].std()/dataPizza['size'].mean()) * 100)
print(f'Coefficient Of Variation Size : {covSize}%')
# Coefficient of Variation Ext Sauce
covExtSauce = ((dataPizza['extra_sauce'].std()/dataPizza['extra_sauce'].mean()) * 100)
print(f'Coefficient Of Variation Sauce : {covExtSauce}%')
# Coefficient of Variation Ext Cheese
covExtCheese = ((dataPizza['extra_cheese'].std()/dataPizza['extra_cheese'].mean()) * 100)
print(f'Coefficient Of Variation Cheese : {covExtCheese}%')


# In[14]:


# Make a correlation data to knowing Value Strength and Direction of Linear Relationship
correlation = dataPizza.corr()
correlation


# ## Z-score

# In[15]:


# Test Value 1
test1 = (dataPizza['price_rupiah'].max() + float(20000))

# Test Value 2
test2 = (dataPizza['price_rupiah'].min() - float(3500))

# Test Value 3
test3 = ((dataPizza['price_rupiah'].max() / dataPizza['price_rupiah'].min()) + float(50000))

# Test Value 4
test4 = ((dataPizza['price_rupiah'].max() / dataPizza['price_rupiah'].min()) - float(50000))


# Standard Score Test 1
standardScore1 = ((test1 - dataPizza['price_rupiah'].mean()) / dataPizza['price_rupiah'].std())
print(f'Z-scores 1 : {standardScore1}')

# Standard Score Test 2
standardScore2 = ((test2 - dataPizza['price_rupiah'].mean()) / dataPizza['price_rupiah'].std())
print(f'Z-scores 2 : {standardScore2}')

# Standard Score Test 3
standardScore3 = ((test3 - dataPizza['price_rupiah'].mean()) / dataPizza['price_rupiah'].std())
print(f'Z-scores 3 : {standardScore3}')

# Standard Score Test 4
standardScore4 = ((test4 - dataPizza['price_rupiah'].mean()) / dataPizza['price_rupiah'].std())
print(f'Z-scores 4 : {standardScore4}')


# In[16]:


dataPizza.sample(2, random_state=1).T


# ## Visualize The Data

# In[17]:


# Setting sns theme
sns.set_theme(color_codes=True, style='darkgrid', palette='deep', font='sans-serif')


# In[18]:


# Constructing a heatmap to understand the correlation
plt.figure(figsize=(10, 10))
sns.heatmap(correlation, cbar=True, square=True, fmt='.1f', annot=True, annot_kws={'size': 8}, cmap='YlGnBu')
plt.plot()


# In[19]:


dataPizza.plot()
plt.show()


# In[20]:


dataPizza.hist(figsize=(12,12))
plt.show()


# In[21]:


sns.regplot(x=dataPizza.price_rupiah, y=dataPizza.diameter, data=dataPizza)
plt.xlim(0, 250000)
plt.ylim(0, 25)
plt.show()


# In[22]:


sns.pairplot(dataPizza)
plt.show()


# ## Splitting the data

# In[23]:


X = dataPizza.drop(['price_rupiah'], axis=1)
y = dataPizza['price_rupiah']


# In[24]:


trainX, testX, trainY, testY = train_test_split(
    X, y,
    test_size=.1,
    shuffle=False,
    random_state=0)


# ## Regression with XGBRegressor

# In[25]:


# XGBoost uses an internal data structure DMatrix - which optimizes both memory effieciency and speed
regressor = XGBRegressor(
    gamma=0,
    learning_rate=0.1,
    max_depth=6,
    n_estimators=1200,
    n_jobs=16,
    objective='reg:squarederror',
    subsample=0.8,
    scale_pos_weight=0,
    reg_alpha=0,
    reg_lambda=1,
    booster='gbtree'
)

model = regressor.fit(trainX, trainY)


# In[26]:


myPipeline = Pipeline(steps=[('model', model)])


# ## Model Evaluate

# In[27]:


# predict X train
trainPredict = myPipeline.predict(trainX)

# predict X test  
testPredict = myPipeline.predict(testX)


# In[28]:


# Train X 

# R Squared 
trainRsquared = metrics.r2_score(trainY, trainPredict)
print(f'R-Squared : {trainRsquared}')

# Mean Absolute Error
trainMAE = metrics.mean_absolute_error(trainY, trainPredict)
print(f'MAE : {trainMAE}')

#  Mean Squared Error
trainMSE = metrics.mean_squared_error(trainY, trainPredict)
print(f'MSE : {trainMSE}')

#  Root Mean Squared Error
trainRMSE = math.sqrt(metrics.mean_squared_error(trainY, trainPredict))
print(f'RMSE : {trainRMSE}')

# Median
trainM = metrics.median_absolute_error(trainY, trainPredict)
print(f'Median : {trainM}')


# In[29]:


# Test X  

# R Squared 
testRsquared = metrics.r2_score(testY, testPredict)
print(f'R-Squared : {testRsquared}')

# Mean Absolute Error
testMAE = metrics.mean_absolute_error(testY, testPredict)
print(f'MAE : {testMAE}')

#  Mean Squared Error
testMSE = metrics.mean_squared_error(testY, testPredict)
print(f'MSE : {testMSE}')

#  Root Mean Squared Error
testRMSE = math.sqrt(metrics.mean_squared_error(testY, testPredict))
print(f'RMSE : {testRMSE}')

# Median
testM = metrics.median_absolute_error(testY, testPredict)
print(f'Median : {testM}')


# In[30]:


# Train Predicted Value & Actual Value
plt.scatter(trainY, trainPredict)
plt.title('Actual Value & Predicted Value')
plt.xlabel('Actual Value')
plt.ylabel('Predicted Value')
plt.xlim(0, 250000)
plt.ylim(0, 250000)
plt.grid(True)
plt.show()


# In[31]:


sns.regplot(x=trainY, y=trainPredict, marker="+")
plt.title('Actual Value & Predicted Value')
plt.xlabel('Actual Value')
plt.ylabel('Predicted Value')
plt.xlim(0, 250000)
plt.ylim(0, 250000)
plt.grid(True)
plt.subplot()
plt.show()


# In[32]:


# Test Predicted Value & Actual Value
plt.scatter(testY, testPredict)
plt.title('Actual Value & Predicted Value')
plt.xlabel('Actual Value')
plt.ylabel('Predicted Value')
plt.xlim(0, 150000)
plt.ylim(0, 150000)
plt.grid(True)
plt.show()


# In[33]:


sns.regplot(x=testY, y=testPredict, marker="+")
plt.title('Actual Value & Predicted Value')
plt.xlabel('Actual Value')
plt.ylabel('Predicted Value')
plt.xlim(0, 150000)
plt.ylim(0, 150000)
plt.grid(True)
plt.subplot()
plt.show()


# ## Prediction

# In[34]:


trainOutput = pd.DataFrame({
    'Train Actual Price': trainY,
    'Train Predicted Price ': trainPredict})
trainOutput.to_csv('/home/aarush100616/Downloads/Projects/Pizza Price Prediction/Train Prediction.csv', index=False)


# In[35]:


testOutput = pd.DataFrame({
    'Test Actual Price': testY,
    'Test Predicted Price ': testPredict})

testOutput.to_csv('/home/aarush100616/Downloads/Projects/Pizza Price Prediction/Test Prediction.csv', index=False)


# In[36]:


trainPredictedOutput = pd.read_csv('/home/aarush100616/Downloads/Projects/Pizza Price Prediction/Train Prediction.csv')
trainPredictedOutput.head(10)


# In[37]:


testPredictedOutput = pd.read_csv('/home/aarush100616/Downloads/Projects/Pizza Price Prediction/Test Prediction.csv')
testPredictedOutput.head(10)

